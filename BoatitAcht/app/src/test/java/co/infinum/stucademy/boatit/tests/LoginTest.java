package co.infinum.stucademy.boatit.tests;

import android.widget.Button;
import android.widget.EditText;

import com.squareup.okhttp.mockwebserver.MockResponse;
import com.squareup.okhttp.mockwebserver.MockWebServer;
import com.squareup.okhttp.mockwebserver.RecordedRequest;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.util.ActivityController;

import java.io.IOException;

import co.infinum.stucademy.boatit.R;
import co.infinum.stucademy.boatit.TestBoatItApplication;
import co.infinum.stucademy.boatit.activities.LoginActivity;
import co.infinum.stucademy.boatit.network.TestApiManager;
import co.infinum.stucademy.boatit.utils.ResourceUtils;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

/**
 * Created by marko on 7/30/15.
 */
@Config(sdk = 21, application = TestBoatItApplication.class)
@RunWith(RobolectricTestRunner.class)
public class LoginTest {

    private MockWebServer mockWebServer;

    @Before
    public void setUp() {
        mockWebServer = TestApiManager.getInstance().getMockWebServer();

    }

    @After
    public void tearDown() {
        try {
            mockWebServer.shutdown();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void successfulLoginTest() {
        mockWebServer.enqueue(new MockResponse()
                .setResponseCode(200)
                .setBody(ResourceUtils.readFromFile("login.json")));

        ActivityController<LoginActivity> loginActivityActivityController = Robolectric.buildActivity(LoginActivity.class);

        LoginActivity loginActivity = loginActivityActivityController.create()
                .start()
                .resume()
                .visible()
                .get();

        EditText passEditText = (EditText) loginActivity.findViewById(R.id.password);
        EditText usernameEditText = (EditText) loginActivity.findViewById(R.id.username);
        Button loginButton = (Button) loginActivity.findViewById(R.id.login);

        usernameEditText.setText("admin@infinum.co");
        passEditText.setText("infinum1");
        loginButton.performClick();

        try {
            RecordedRequest request = mockWebServer.takeRequest();

            assertThat(request.getHeader("Content-Type"), equalTo("application/json; charset=UTF-8"));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
