package co.infinum.stucademy.boatit.network;

import java.util.concurrent.Executor;

/**
 * Created by marko on 7/30/15.
 */
public class SingleThreadExecutor implements Executor {
    @Override
    public void execute(Runnable command) {
        command.run();
    }
}
