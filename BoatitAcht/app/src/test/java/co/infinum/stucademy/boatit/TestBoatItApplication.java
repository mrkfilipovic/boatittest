package co.infinum.stucademy.boatit;


import co.infinum.stucademy.boatit.network.TestApiManager;

public class TestBoatItApplication extends BoatItApplication {


    public void init() {
        TestApiManager.getInstance().init();

        apiManager = TestApiManager.getInstance();
    }
}