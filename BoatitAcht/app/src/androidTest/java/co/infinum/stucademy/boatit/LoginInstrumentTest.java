package co.infinum.stucademy.boatit;

import android.app.Activity;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.Espresso;
import android.support.test.espresso.action.ViewActions;
import android.support.test.espresso.assertion.ViewAssertions;
import android.support.test.espresso.matcher.ViewMatchers;
import android.test.ActivityInstrumentationTestCase2;

import org.junit.Test;

import co.infinum.stucademy.boatit.activities.LoginActivity;

/**
 * Created by marko on 7/30/15.
 */
public class LoginInstrumentTest extends ActivityInstrumentationTestCase2<LoginActivity> {

    private Activity activity;

    public LoginInstrumentTest() {
        super(LoginActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        injectInstrumentation(InstrumentationRegistry.getInstrumentation());
        activity = getActivity();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    @Test
    public void testSuccessfulLogin() {
        Espresso.onView(ViewMatchers.withId(R.id.username)).perform(ViewActions.typeText("admin@infinum.co"));
        Espresso.onView(ViewMatchers.withId(R.id.password)).perform(
                ViewActions.typeText("infinum1"),
                ViewActions.closeSoftKeyboard()
        );
        Espresso.onView(ViewMatchers.withText(R.string.remember_me)).check(ViewAssertions.matches(ViewMatchers.isNotChecked()));
        Espresso.onView(ViewMatchers.isRoot()).perform(ViewActions.swipeDown());
        Espresso.onView(ViewMatchers.withText(R.string.login)).perform(ViewActions.click());
        Espresso.onView(ViewMatchers.withText(R.string.please_wait)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()));
    }}
