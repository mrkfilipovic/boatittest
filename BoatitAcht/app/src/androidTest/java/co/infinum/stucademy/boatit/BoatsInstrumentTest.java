package co.infinum.stucademy.boatit;

import android.app.Activity;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.Espresso;
import android.support.test.espresso.action.ViewActions;
import android.support.test.espresso.assertion.ViewAssertions;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.espresso.matcher.ViewMatchers;
import android.test.ActivityInstrumentationTestCase2;
import android.view.View;

import org.junit.Test;

import java.util.regex.Matcher;

import co.infinum.stucademy.boatit.activities.BoatsActivity;
import co.infinum.stucademy.boatit.models.Boat;

/**
 * Created by marko on 7/30/15.
 */
public class BoatsInstrumentTest extends ActivityInstrumentationTestCase2<BoatsActivity> {

    private Activity activity;

    public BoatsInstrumentTest() {
        super(BoatsActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        injectInstrumentation(InstrumentationRegistry.getInstrumentation());
        activity = getActivity();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    @Test
    public void testSuccessfulBoatsLoaded() {
        Espresso.onView(ViewMatchers.withText(R.string.please_wait)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()));
        Espresso.onView(ViewMatchers.withId(R.id.new_comment)).check(ViewAssertions.matches(ViewMatchers.isDisplayed())).perform(ViewActions.click());
        Espresso.onView(ViewMatchers.withId(R.id.boat_list)).check(ViewAssertions.matches(ViewMatchers.isDisplayed())).perform(RecyclerViewActions.scrollToPosition(4)).perform(RecyclerViewActions.actionOnItemAtPosition(5, ViewActions.click()));
    }
}
