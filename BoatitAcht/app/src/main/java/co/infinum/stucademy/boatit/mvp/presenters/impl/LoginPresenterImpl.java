package co.infinum.stucademy.boatit.mvp.presenters.impl;

import android.content.Context;

import co.infinum.stucademy.boatit.models.User;
import co.infinum.stucademy.boatit.mvp.interactors.impl.LoginInteractorImpl;
import co.infinum.stucademy.boatit.mvp.listeners.LoudLoginListener;
import co.infinum.stucademy.boatit.mvp.listeners.RememberListener;
import co.infinum.stucademy.boatit.mvp.presenters.LoginPresenter;
import co.infinum.stucademy.boatit.mvp.views.LoginView;
import co.infinum.stucademy.boatit.utilities.NetworkUtils;

/**
 * Created by marko on 7/29/15.
 */
public class LoginPresenterImpl implements LoginPresenter {
    private final LoginView view;
    private final LoginInteractorImpl interactor;
    private Context context;

    @Override
    public void isRemembered() {
        interactor.checkRememberance(context, rememberListener);
    }

    public LoginPresenterImpl(LoginView view, LoginInteractorImpl interactor, Context context) {

        this.view = view;
        this.interactor = interactor;
        this.context = context;
    }

    @Override
    public void login(String username, String password, boolean remember) {
        view.showProgress();
        interactor.checkLoginCredentials(username, password, remember, context, listener);
    }

    @Override
    public void registration() {
        if (NetworkUtils.connected()) {
            view.showRegistrationActivity();
        } else {
            view.noConnectivity();

        }
    }

    private LoudLoginListener listener = new LoudLoginListener() {
        @Override
        public void onLoginSuccessful() {
            view.hideProgress();
            view.showBoatList();
        }

        @Override
        public void onLoginFailed(String message) {
            view.hideProgress();
            view.showLoginError(message);
        }
    };

    private RememberListener rememberListener = new RememberListener() {
        @Override
        public void isRemembered(User user) {
            view.initData(user);
        }
    };


}
