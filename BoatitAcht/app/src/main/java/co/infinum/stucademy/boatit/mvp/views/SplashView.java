package co.infinum.stucademy.boatit.mvp.views;

/**
 * Created by marko on 7/29/15.
 */
public interface SplashView extends BaseView {

    void showBoatActivity();

    void showLoginActivity();
}
