package co.infinum.stucademy.boatit.mvp.presenters.impl;

import java.util.List;

import co.infinum.stucademy.boatit.models.Boat;
import co.infinum.stucademy.boatit.mvp.interactors.BoatsInteractor;
import co.infinum.stucademy.boatit.mvp.listeners.BoatAddedListener;
import co.infinum.stucademy.boatit.mvp.listeners.BoatsListener;
import co.infinum.stucademy.boatit.mvp.presenters.BoatsPresenter;
import co.infinum.stucademy.boatit.mvp.views.BoatsView;

/**
 * Created by marko on 7/27/15.
 */
public class BoatsPresenterImpl implements BoatsPresenter {

    private final BoatsView view;
    private final BoatsInteractor boatsInteractor;

    public BoatsPresenterImpl(BoatsView view, BoatsInteractor boatsInteractor) {
        this.view = view;
        this.boatsInteractor = boatsInteractor;
    }

    @Override
    public void getBoats() {
        view.showProgress();
        boatsInteractor.getBoats(listener);
    }

    private BoatsListener listener = new BoatsListener() {
        @Override
        public void onBoatsReceived(List<Boat> boats) {
            view.hideProgress();
            view.onBoatListReceived(boats);
        }

        @Override
        public void onListEmpty() {
            view.hideProgress();
            view.onBoatListEmpty();
        }
    };

    @Override
    public void onBoatClicked(Boat boat) {
        view.openBoatDetails(boat);
    }

    @Override
    public void addNewBoat(Boat boat) {
        boatsInteractor.addNewBoat(boat, boatAddedListener);
    }

    private BoatAddedListener boatAddedListener = new BoatAddedListener() {
        @Override
        public void onBoatAddedSuccess() {
            view.reloadBoatList();
        }
    };
}
