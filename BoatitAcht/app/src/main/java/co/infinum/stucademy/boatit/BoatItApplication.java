package co.infinum.stucademy.boatit;

import android.app.Application;

import com.raizlabs.android.dbflow.config.FlowManager;

import co.infinum.stucademy.boatit.network.ApiManager;
import co.infinum.stucademy.boatit.network.ApiManagerImpl;
import co.infinum.stucademy.boatit.network.BoatItService;

/**
 * Created by marko on 7/21/15.
 */
public class BoatItApplication extends Application {

    private static BoatItApplication instance;
    protected ApiManager apiManager;


    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        FlowManager.init(this);
        init();
    }

    protected void init() {
        apiManager = ApiManagerImpl.getInstance();
    }

    public static BoatItApplication getInstance() {
        return instance;
    }

    public static BoatItService getApiService() {
        return getInstance().apiManager.getService();
    }

}
