package co.infinum.stucademy.boatit.models.responses;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import co.infinum.stucademy.boatit.models.Boat;

/**
 * Created by marko on 7/23/15.
 */
public class BoatsResponse {

    @SerializedName("response")
    private List<Boat> boats;

    public List<Boat> getBoats() {
        return boats;
    }
}
