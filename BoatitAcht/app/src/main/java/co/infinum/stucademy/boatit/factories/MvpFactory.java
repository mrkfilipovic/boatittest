package co.infinum.stucademy.boatit.factories;

import android.content.Context;

import co.infinum.stucademy.boatit.mvp.interactors.impl.BoatDetailsInteractorImpl;
import co.infinum.stucademy.boatit.mvp.interactors.impl.BoatsInteractorImpl;
import co.infinum.stucademy.boatit.mvp.interactors.impl.LoginInteractorImpl;
import co.infinum.stucademy.boatit.mvp.interactors.impl.SplashInteractorImpl;
import co.infinum.stucademy.boatit.mvp.presenters.BoatDetailsPresenter;
import co.infinum.stucademy.boatit.mvp.presenters.BoatsPresenter;
import co.infinum.stucademy.boatit.mvp.presenters.LoginPresenter;
import co.infinum.stucademy.boatit.mvp.presenters.SplashPresenter;
import co.infinum.stucademy.boatit.mvp.presenters.impl.BoatDetailsPresenterImpl;
import co.infinum.stucademy.boatit.mvp.presenters.impl.BoatsPresenterImpl;
import co.infinum.stucademy.boatit.mvp.presenters.impl.LoginPresenterImpl;
import co.infinum.stucademy.boatit.mvp.presenters.impl.SplashPresenterImpl;
import co.infinum.stucademy.boatit.mvp.views.BoatDetailsView;
import co.infinum.stucademy.boatit.mvp.views.BoatsView;
import co.infinum.stucademy.boatit.mvp.views.LoginView;
import co.infinum.stucademy.boatit.mvp.views.SplashView;

/**
 * Created by marko on 7/27/15.
 */
public class MvpFactory {

    public static BoatsPresenter getPresenter(BoatsView view) {
        return new BoatsPresenterImpl(view, new BoatsInteractorImpl());
    }

    public static BoatDetailsPresenter getPresenter(BoatDetailsView view) {
        return new BoatDetailsPresenterImpl(view, new BoatDetailsInteractorImpl());
    }

    public static SplashPresenter getPresenter(SplashView view, Context context) {
        return new SplashPresenterImpl(view, new SplashInteractorImpl(), context);
    }

    public static LoginPresenter getPresenter(LoginView view, Context context) {
        return new LoginPresenterImpl(view, new LoginInteractorImpl(), context);
    }
}
