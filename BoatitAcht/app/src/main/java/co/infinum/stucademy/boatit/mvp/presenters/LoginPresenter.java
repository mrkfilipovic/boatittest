package co.infinum.stucademy.boatit.mvp.presenters;

/**
 * Created by marko on 7/29/15.
 */
public interface LoginPresenter {

    void registration();

    void login(String username, String password, boolean remember);

    void isRemembered();
}
