package co.infinum.stucademy.boatit.mvp.views;

import java.util.List;

import co.infinum.stucademy.boatit.models.Boat;

/**
 * Created by marko on 7/27/15.
 */
public interface BoatsView extends BaseView {

    void onBoatListReceived(List<Boat> boats);

    void onBoatListEmpty();

    void openBoatDetails(Boat boat);

    void reloadBoatList();
}
