package co.infinum.stucademy.boatit.mvp.listeners;

import co.infinum.stucademy.boatit.models.User;

/**
 * Created by marko on 7/29/15.
 */
public interface RememberListener {

    void isRemembered(User user);
}
