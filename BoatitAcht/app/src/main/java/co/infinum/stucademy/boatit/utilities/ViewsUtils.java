package co.infinum.stucademy.boatit.utilities;

import android.text.TextUtils;
import android.widget.EditText;

import co.infinum.stucademy.boatit.R;

/**
 * Created by marko on 7/27/15.
 */
public class ViewsUtils {

    public static String getTextFrom(EditText editText) {
        String edited = editText.getText().toString();

        if (TextUtils.isEmpty(edited)) {
            editText.setError(editText.getContext().getString(R.string.empty_content_error));
            return null;
        }
        return edited;
    }
}
