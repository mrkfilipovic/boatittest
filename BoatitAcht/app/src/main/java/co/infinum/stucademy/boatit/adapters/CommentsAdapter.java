package co.infinum.stucademy.boatit.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import co.infinum.stucademy.boatit.R;
import co.infinum.stucademy.boatit.interfaces.VoteClickListener;
import co.infinum.stucademy.boatit.models.Boat;
import co.infinum.stucademy.boatit.models.Comment;

/**
 * Created by marko on 7/23/15.
 */
public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.CommentViewHolder> {

    private final int HEADER_VIEW_TYPE = 1;

    private final int NORMAL_VIEW_TYPE = 2;

    private Context context;
    private List<Comment> commentList;
    private Boat boat;

    private TextView upVote;
    private TextView downVote;
    private TextView score;

    private VoteClickListener listener;
    private View.OnClickListener voteListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            listener.voteClicked(v);
        }
    };

    public CommentsAdapter(Context context, Boat boat, List<Comment> commentList) {
        this.context = context;
        this.listener = (VoteClickListener) context;
        this.commentList = commentList;
        this.boat = boat;
    }

    @Override
    public CommentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        switch (viewType) {
            case HEADER_VIEW_TYPE:
                view = LayoutInflater.from(context).inflate(R.layout.list_item_boat_details, parent, false);
                break;
            case NORMAL_VIEW_TYPE:
                view = LayoutInflater.from(context).inflate(R.layout.list_item_comments, parent, false);
                break;
        }
        return new CommentViewHolder(view, viewType);

    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return HEADER_VIEW_TYPE;
        }
        return NORMAL_VIEW_TYPE;
    }

    @Override
    public void onBindViewHolder(CommentViewHolder holder, int position) {

        if (position == 0) {
            Glide.with(context).load(boat.getBoatImage()).dontAnimate().placeholder(R.drawable.ic_logo).into(holder.image).onLoadFailed(null, context.getResources().getDrawable(R.drawable.ic_logo));
            upVote = holder.upVote;
            upVote.setOnClickListener(voteListener);
            downVote = holder.downVote;
            downVote.setOnClickListener(voteListener);
            score = holder.score;
            score.setText(String.format("%d", boat.getScore()));
        } else {
            Comment comment = commentList.get(position - 1);
            holder.nameTime.setText(comment.getAuthor().getFirstName() + " " + comment.getAuthor().getLastName() + ", " + comment.getDateCreated());
            holder.content.setText(comment.getContent());
        }
    }

    @Override
    public int getItemCount() {
        return commentList.size() + 1;
    }

    public void changeStatus(int score, int viewId) {
        this.score.setText(String.format("%d", score));
        switch (viewId) {
            case R.id.boat_upvote:
                changeStatus(downVote, upVote);
                break;
            case R.id.boat_downvote:
                changeStatus(upVote, downVote);
        }
    }

    private void changeStatus(TextView enable, TextView disable) {
        disable.setTextColor(context.getResources().getColor(R.color.primary_dark));
        disable.setClickable(false);
        enable.setTextColor(context.getResources().getColor(R.color.black));
        enable.setClickable(true);
    }

//    private void changeStatus(TextView textView) {
//        int color;
//        boolean clicked = !textView.isClickable();
//        if (clicked) {
//            color = R.color.black;
//        } else {
//            color = R.color.primary_dark;
//        }
//        textView.setTextColor(context.getResources().getColor(color));
//        textView.setClickable(clicked);
//    }

    public class CommentViewHolder extends RecyclerView.ViewHolder {
        TextView nameTime;
        TextView content;
        ImageView image;
        TextView downVote;
        TextView upVote;
        TextView score;

        public CommentViewHolder(View view, int viewType) {
            super(view);
            switch (viewType) {
                case HEADER_VIEW_TYPE:
                    image = (ImageView) view.findViewById(R.id.boat_image);
                    downVote = (TextView) view.findViewById(R.id.boat_downvote);
                    upVote = (TextView) view.findViewById(R.id.boat_upvote);
                    score = (TextView) view.findViewById(R.id.score);
                    break;
                case NORMAL_VIEW_TYPE:
                    nameTime = (TextView) view.findViewById(R.id.comment_name_time);
                    content = (TextView) view.findViewById(R.id.comment_content);
                    break;
            }
        }

    }
}
