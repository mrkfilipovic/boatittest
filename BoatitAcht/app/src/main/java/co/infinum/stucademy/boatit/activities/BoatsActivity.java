package co.infinum.stucademy.boatit.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import co.infinum.stucademy.boatit.R;
import co.infinum.stucademy.boatit.adapters.BoatsAdapter;
import co.infinum.stucademy.boatit.dialogs.AddNewContentFragment;
import co.infinum.stucademy.boatit.factories.MvpFactory;
import co.infinum.stucademy.boatit.interfaces.BoatListener;
import co.infinum.stucademy.boatit.models.Boat;
import co.infinum.stucademy.boatit.models.Comment;
import co.infinum.stucademy.boatit.mvp.presenters.BoatsPresenter;
import co.infinum.stucademy.boatit.mvp.views.BoatsView;

/**
 * Created by marko on 7/22/15.
 */
public class BoatsActivity extends BaseActivity implements BoatListener, BoatsView, AddNewContentFragment.DataListener {

    private static final String BOAT_LIST = "boat_list";

    @Bind(R.id.boat_list)
    RecyclerView recyclerView;

    private ArrayList<Boat> boatList;
    private BoatsPresenter presenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_boats);
        ButterKnife.bind(this);
        presenter = MvpFactory.getPresenter(this);
        reloadBoatList();

        setTitle(getString(R.string.title_boats_activity));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.new_comment:
                AddNewContentFragment dialog = AddNewContentFragment.newInstance(AddNewContentFragment.BOAT);
                dialog.show(getFragmentManager(), "");
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBoatClick(Boat boat) {
        presenter.onBoatClicked(boat);
    }

    @Override
    public void onBoatListReceived(List<Boat> boats) {
        recyclerView.setAdapter(new BoatsAdapter(boats, this, this));
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
    }

    @Override
    public void noConnectivity() {
        super.connectivityError();
    }

    @Override
    public void onBoatListEmpty() {
        super.showAlert(getString(R.string.error_empty_list));
    }

    @Override
    public void showProgress() {
        super.showLoader();
    }

    @Override
    public void hideProgress() {
        super.hideLoader();
    }

    @Override
    public void error() {
        super.showAlert(getString(R.string.error_boats));
    }

    @Override
    public void openBoatDetails(Boat boat) {
        Intent intent = new Intent(BoatsActivity.this, BoatDetailsActivity.class);
        intent.putExtra(getString(R.string.boat), boat);
        startNextActivity(intent, false);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (boatList != null) {
            outState.putParcelableArrayList(BOAT_LIST, boatList);
        }
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onNewAdded(Comment comment) {
        // ignored method...
    }

    @Override
    public void reloadBoatList() {
        presenter.getBoats();
    }

    @Override
    public void onNewAdded(Boat boat) {
        presenter.addNewBoat(boat);
    }
}
