package co.infinum.stucademy.boatit.database;

import android.os.Handler;

import com.raizlabs.android.dbflow.sql.language.Select;
import com.raizlabs.android.dbflow.structure.Model;
import com.raizlabs.android.dbflow.structure.ModelAdapter;

import java.util.List;

import co.infinum.stucademy.boatit.models.Boat;
import co.infinum.stucademy.boatit.models.Comment;
import co.infinum.stucademy.boatit.models.User;

/**
 * Created by marko on 7/25/15.
 */
public class BoatDBOperationsImpl implements BoatDBOperations {

    @Override
    public <T extends Model> void update(T obj) {
        obj.update();
    }

    @Override
    public void addBoats(final List<Boat> boatList) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                for (Boat boat : boatList) {
                    boat.save();
                    addAllComments(boat);
                }

            }
        }).start();

        }

        @Override
        public List<Boat> getAllBoats () {
            return new Select().from(Boat.class).queryList();
        }

        @Override
        public void addBoat (Boat boat){
            boat.save();
        }

        @Override
        public void addAllComments (Boat boat){
            for (Comment comment : boat.getComments()) {
                comment.setBoat(boat);
                comment.save();
            }
        }

        @Override
        public List<Comment> getAllComments ( long boatId){
            return new Select().from(Comment.class).where("boat_id = ?", boatId).queryList();
        }

        @Override
        public void addCreator (User user){

        }

        @Override
        public User getCreator ( long creatorId){
            return new Select().from(User.class).where("id = ?", creatorId).querySingle();
        }
    }
