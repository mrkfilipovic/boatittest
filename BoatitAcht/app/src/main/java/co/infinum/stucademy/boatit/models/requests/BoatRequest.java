package co.infinum.stucademy.boatit.models.requests;

import com.google.gson.annotations.SerializedName;

import co.infinum.stucademy.boatit.models.Boat;

/**
 * Created by marko on 7/29/15.
 */
public class BoatRequest {

    @SerializedName("post")
    private Post post;

    public BoatRequest(Boat boat) {
        this.post = new Post(boat.getBoatName(), boat.getBoatImage());
    }

    public class Post {
        @SerializedName("title")
        private String title;
        @SerializedName("image_url")
        private String imageUrl;

        public Post(String title, String imageUrl) {
            this.title = title;
            this.imageUrl = imageUrl;
        }
    }
}
