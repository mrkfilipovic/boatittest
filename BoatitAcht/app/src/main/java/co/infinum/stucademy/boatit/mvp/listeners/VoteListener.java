package co.infinum.stucademy.boatit.mvp.listeners;

/**
 * Created by marko on 7/28/15.
 */
public interface VoteListener {
    void onVoteResponse(int score, int viewId);
}
