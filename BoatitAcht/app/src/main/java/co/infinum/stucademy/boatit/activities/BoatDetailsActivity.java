package co.infinum.stucademy.boatit.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import co.infinum.stucademy.boatit.R;
import co.infinum.stucademy.boatit.adapters.CommentsAdapter;
import co.infinum.stucademy.boatit.dialogs.AddNewContentFragment;
import co.infinum.stucademy.boatit.factories.MvpFactory;
import co.infinum.stucademy.boatit.interfaces.VoteClickListener;
import co.infinum.stucademy.boatit.models.Boat;
import co.infinum.stucademy.boatit.models.Comment;
import co.infinum.stucademy.boatit.mvp.presenters.BoatDetailsPresenter;
import co.infinum.stucademy.boatit.mvp.views.BoatDetailsView;

public class BoatDetailsActivity extends BaseActivity implements VoteClickListener, BoatDetailsView, AddNewContentFragment.DataListener {

    // what to use instead?
    private static final int STATUS_OK = 200;
    private static final String BOAT = "boat";

    @Bind(R.id.comments_listview)
    RecyclerView recyclerView;

    Boat boat;
    CommentsAdapter adapter;

    private BoatDetailsPresenter presenter;
    private List<Comment> comments;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_boat_details);
        ButterKnife.bind(this);
        presenter = MvpFactory.getPresenter(this);

        checkOutState();
        init();


        setBackEnabled(true);
    }

    private void checkOutState() {
        // if (savedInstanceState != null) {
        // boat = savedInstanceState.getParcelable(BOAT);
        // }
    }

    @Override
    public void error() {
        showAlert(getString(R.string.error_boat_details));
    }

    private void init() {
        Intent boatIntent = getIntent();
        if (boatIntent != null && boatIntent.getParcelableExtra(getString(R.string.boat)) != null) {
            boat = boatIntent.getParcelableExtra(getString(R.string.boat));
            presenter.onIntentReceived(boat);
        } else {
            presenter.onIntentDefect();
        }
    }

    @Override
    public void setupView(Boat boat) {
        comments = boat.getComments();
        adapter = new CommentsAdapter(this, boat, comments);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_boat_details, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.new_comment:
                AddNewContentFragment dialog = AddNewContentFragment.newInstance(AddNewContentFragment.COMMENT);
                dialog.show(getFragmentManager(), "");
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setupVotes(int score, int viewId) {
        adapter.changeStatus(score, viewId);
    }

    @Override
    public void setTitle(int title, String boatName) {
        super.setTitle(String.format(getString(title), boatName));
    }

    @Override
    public void onBoatFound(Boat boat) {
        comments.clear();
        comments.addAll(boat.getComments());
        this.boat = boat;

        // TODO boat image disappears after notification
        adapter.notifyDataSetChanged();
    }

    @Override
    public void noConnectivity() {
        super.connectivityError();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(BOAT, boat);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void voteClicked(final View view) {
        presenter.onVoteClicked(view.getId(), (int) boat.getId());
    }

    @Override
    public void showProgress() {
        super.showLoader();
    }

    @Override
    public void hideProgress() {
        super.hideLoader();
    }

    @Override
    public void reloadComments() {
        presenter.reloadBoat(boat.getId());
    }

    @Override
    public void onNewAdded(Comment comment) {
        presenter.addComment(comment, boat.getId());
    }

    @Override
    public void onNewAdded(Boat boat) {
        // hmm... this didn't go out too well...
        // TODO make 2 dialogs...
    }
}
