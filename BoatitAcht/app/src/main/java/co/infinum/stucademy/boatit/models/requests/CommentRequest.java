package co.infinum.stucademy.boatit.models.requests;

import com.google.gson.annotations.SerializedName;

import co.infinum.stucademy.boatit.models.Comment;

/**
 * Created by marko on 7/28/15.
 */
public class CommentRequest {
    @SerializedName("comment")
    CommentContent comment;

    public CommentRequest(Comment comment) {
        this.comment = new CommentContent(comment.getContent());
    }

    public class CommentContent {
        @SerializedName("content")
        String content;

        public CommentContent(String content) {
            this.content = content;
        }
    }
}
