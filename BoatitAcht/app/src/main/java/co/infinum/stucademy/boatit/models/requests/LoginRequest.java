package co.infinum.stucademy.boatit.models.requests;

import com.google.gson.annotations.SerializedName;

/**
 * Created by marko on 7/21/15.
 */
public class LoginRequest {
    @SerializedName("email")
    private String username;

    @SerializedName("password")
    private String password;

    public LoginRequest(String username, String password) {
        this.username = username;
        this.password = password;
    }
}
