package co.infinum.stucademy.boatit.mvp.interactors.impl;

import android.content.Context;
import android.text.TextUtils;

import co.infinum.stucademy.boatit.BoatItApplication;
import co.infinum.stucademy.boatit.R;
import co.infinum.stucademy.boatit.models.User;
import co.infinum.stucademy.boatit.models.requests.LoginRequest;
import co.infinum.stucademy.boatit.models.responses.LoginResponse;
import co.infinum.stucademy.boatit.mvp.interactors.LoginInteractor;
import co.infinum.stucademy.boatit.mvp.listeners.LoudLoginListener;
import co.infinum.stucademy.boatit.mvp.listeners.RememberListener;
import co.infinum.stucademy.boatit.network.ApiManagerImpl;
import co.infinum.stucademy.boatit.utilities.NetworkUtils;
import co.infinum.stucademy.boatit.utilities.SharedPreferencesUtils;
import retrofit.Callback;
import retrofit.RetrofitError;

/**
 * Created by marko on 7/29/15.
 */
public class LoginInteractorImpl implements LoginInteractor {


    @Override
    public void checkRememberance(Context context, RememberListener rememberListener) {
        if (SharedPreferencesUtils.getRemember(context)) {
            User user = SharedPreferencesUtils.getUser(context);
            if (user != null) {
                rememberListener.isRemembered(user);
            }
        }
    }

    @Override
    public void checkLoginCredentials(String username, String password, final boolean remember, final Context context, final LoudLoginListener listener) {
        if (NetworkUtils.connected()) {
            final User user = new User(username, password);

            BoatItApplication.getApiService().login(new LoginRequest(user.getUsername(), user.getPassword()), new Callback<LoginResponse>() {
                @Override
                public void success(LoginResponse loginResponse, retrofit.client.Response response) {
                    switch (response.getStatus()) {
                        case 200:
                            SharedPreferencesUtils.setToken(loginResponse.getResponse().getToken(), context);
                            SharedPreferencesUtils.saveUser(user, context);
                            SharedPreferencesUtils.rememberUser(remember, context);
                            listener.onLoginSuccessful();
                            break;
                        case 401:
                            listener.onLoginFailed(response.getReason());
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    listener.onLoginFailed(error.getMessage());
                }
            });
        } else {
            User user = SharedPreferencesUtils.getUser(context);
            if (user != null) {
                if (TextUtils.equals(username, user.getUsername()) && TextUtils.equals(password, user.getPassword())) {
                    listener.onLoginSuccessful();
                } else {
                    listener.onLoginFailed(context.getString(R.string.invalid_credentials));
                }
            }
        }
    }
}
