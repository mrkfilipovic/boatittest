package co.infinum.stucademy.boatit.database;

import com.raizlabs.android.dbflow.structure.Model;

import java.util.List;

import co.infinum.stucademy.boatit.models.Boat;
import co.infinum.stucademy.boatit.models.Comment;
import co.infinum.stucademy.boatit.models.User;

/**
 * Created by marko on 7/25/15.
 */
public interface BoatDBOperations {

    void addBoats(List<Boat> boatList);

    List<Boat> getAllBoats();

    void addBoat(Boat boat);

    <T extends Model> void update(T obj);

    void addAllComments(Boat boat);

    List<Comment> getAllComments(long boatId);

    void addCreator(User user);

    User getCreator(long creatorId);
}