package co.infinum.stucademy.boatit.mvp.listeners;

import co.infinum.stucademy.boatit.models.Boat;

/**
 * Created by marko on 7/28/15.
 */
public interface BoatResponseListener {
    void onBoatReceived(Boat boat);
}
