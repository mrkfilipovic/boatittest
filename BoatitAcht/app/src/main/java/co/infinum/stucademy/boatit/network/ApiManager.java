package co.infinum.stucademy.boatit.network;

/**
 * Created by marko on 7/30/15.
 */
public interface ApiManager {
    BoatItService getService();
}
