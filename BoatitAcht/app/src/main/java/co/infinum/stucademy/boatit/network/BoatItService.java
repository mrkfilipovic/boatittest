package co.infinum.stucademy.boatit.network;

import com.squareup.okhttp.Response;

import co.infinum.stucademy.boatit.models.Comment;
import co.infinum.stucademy.boatit.models.requests.BoatRequest;
import co.infinum.stucademy.boatit.models.requests.CommentRequest;
import co.infinum.stucademy.boatit.models.requests.LoginRequest;
import co.infinum.stucademy.boatit.models.requests.RegisterRequest;
import co.infinum.stucademy.boatit.models.responses.BoatResponse;
import co.infinum.stucademy.boatit.models.responses.BoatsResponse;
import co.infinum.stucademy.boatit.models.responses.LoginResponse;
import co.infinum.stucademy.boatit.models.responses.VoteResponse;
import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;

/**
 * Created by marko on 7/21/15.
 */
public interface BoatItService {

    @POST("/users/login")
    void login(@Body LoginRequest request, Callback<LoginResponse> loginCallback);

    @POST("/users/register")
    void register(@Body RegisterRequest registerRequest, Callback<LoginResponse> registerCallback);

    @GET("/posts")
    void getBoats(Callback<BoatsResponse> boatResponseCallback);

    @GET("/posts/{id}/upboat")
    void boatUpvote(@Path("id") long id, Callback<VoteResponse> upvoteResponseCallback);

    @GET("/posts/{id}/downboat")
    void boatDownvote(@Path("id") long id, Callback<VoteResponse> downvoteResponseCallback);

    @GET("/posts/{id}")
    void getBoat(@Path("id") long id, Callback<BoatResponse> boatResponseCallback);

    @POST("/posts/{post_id}/comments")
    void addComment(@Path("post_id") long boatId, @Body CommentRequest comment, Callback<Response> callback);

    @POST("/posts")
    void addBoat(@Body BoatRequest boatRequest, Callback<Response> callback);
}
