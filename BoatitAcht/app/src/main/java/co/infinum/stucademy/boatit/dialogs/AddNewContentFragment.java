package co.infinum.stucademy.boatit.dialogs;

import android.app.Activity;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.infinum.stucademy.boatit.R;
import co.infinum.stucademy.boatit.models.Boat;
import co.infinum.stucademy.boatit.models.Comment;
import co.infinum.stucademy.boatit.utilities.ViewsUtils;

public class AddNewContentFragment extends DialogFragment {

    private static final String ITEM_TYPE = "param1";
    public static final String COMMENT = "comment";
    public static final String BOAT = "boat";

    @Bind(R.id.content)
    EditText contentEditText;

    @Bind(R.id.image_url)
    EditText imageUrlEditText;

    @Bind(R.id.content_input_layout)
    TextInputLayout contentTextLayout;

    private String itemType;
    private DataListener mListener;

    public static AddNewContentFragment newInstance(String itemType) {
        AddNewContentFragment fragment = new AddNewContentFragment();
        Bundle args = new Bundle();
        args.putString(ITEM_TYPE, itemType);
        fragment.setArguments(args);
        return fragment;
    }

    public AddNewContentFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            itemType = getArguments().getString(ITEM_TYPE);
        }

        setStyle(STYLE_NO_TITLE, 0);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_new, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        switch (itemType) {
            case BOAT:
                contentTextLayout.setHint("Boat name");
                break;
            case COMMENT:
                contentTextLayout.setHint("Text here");
                imageUrlEditText.setVisibility(View.GONE);
                break;
        }
    }

    @OnClick(R.id.save_button)
    public void onsave() {
        if (mListener != null) {
            switch (itemType) {
                case BOAT:
                    String name = ViewsUtils.getTextFrom(contentEditText);
                    String imageUrl = ViewsUtils.getTextFrom(imageUrlEditText);
                    mListener.onNewAdded(new Boat(name, imageUrl));
                    break;
                case COMMENT:
                    String commentText = ViewsUtils.getTextFrom(contentEditText);
                    mListener.onNewAdded(new Comment(commentText));
                    break;
            }
            dismiss();
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (DataListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface DataListener {
        void onNewAdded(Comment comment);

        void onNewAdded(Boat boat);
    }

}
