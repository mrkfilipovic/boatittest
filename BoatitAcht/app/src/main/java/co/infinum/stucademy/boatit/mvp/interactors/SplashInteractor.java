package co.infinum.stucademy.boatit.mvp.interactors;

import android.content.Context;

import co.infinum.stucademy.boatit.mvp.listeners.SilentLoginListener;

/**
 * Created by marko on 7/29/15.
 */
public interface SplashInteractor {
    void checkSavedData(Context context, SilentLoginListener listener);
}
