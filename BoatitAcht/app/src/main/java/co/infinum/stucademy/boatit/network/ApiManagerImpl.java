package co.infinum.stucademy.boatit.network;

import android.util.Log;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.raizlabs.android.dbflow.structure.ModelAdapter;
import com.squareup.okhttp.OkHttpClient;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.util.Date;

import co.infinum.stucademy.boatit.network.deserializers.DateDeserializer;
import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;

/**
 * Created by marko on 7/21/15.
 */
public class ApiManagerImpl implements ApiManager {

    public static final String API_ENDPOINT = "https://boatit.infinum.co";
    public static final String API_SUB = "/api/v1";
    private static final String TAG = "Network";

    private static final CookieHandler COOKIE_HANDLER = new CookieManager();
    private static final OkHttpClient OK_HTTP_CLIENT = new OkHttpClient().setCookieHandler(COOKIE_HANDLER);

    private static final Gson GSON = new GsonBuilder()
            .registerTypeAdapter(Date.class, new DateDeserializer())
            .setExclusionStrategies(new ExclusionStrategy() {
                @Override
                public boolean shouldSkipField(FieldAttributes f) {
                    return f.getDeclaredClass().equals(ModelAdapter.class);
                }

                @Override
                public boolean shouldSkipClass(Class<?> clazz) {
                    return false;
                }
            })
            .create();

    private static final RestAdapter.Log LOG = new RestAdapter.Log() {
        @Override
        public void log(String message) {
            Log.d(TAG, message);
        }
    };

    private static final RestAdapter REST_ADAPTER = new RestAdapter.Builder()
            .setLog(LOG)
            .setClient(new OkClient(OK_HTTP_CLIENT))
            .setEndpoint(API_ENDPOINT + API_SUB)
            .setRequestInterceptor(new Interceptor())
            .setConverter(new GsonConverter(GSON))
            .setLogLevel(RestAdapter.LogLevel.FULL)
            .build();

    private static final BoatItService BOAT_IT_SERVICE = REST_ADAPTER.create(BoatItService.class);

    private static ApiManagerImpl instance;

    @Override
    public BoatItService getService() {
        return BOAT_IT_SERVICE;
    }

    public synchronized static ApiManagerImpl getInstance() {
        if (instance == null) {
            instance = new ApiManagerImpl();
        }
        return instance;
    }
}
