package co.infinum.stucademy.boatit.mvp.interactors;

import co.infinum.stucademy.boatit.models.Comment;
import co.infinum.stucademy.boatit.mvp.listeners.BoatResponseListener;
import co.infinum.stucademy.boatit.mvp.listeners.CommentListener;
import co.infinum.stucademy.boatit.mvp.listeners.VoteListener;

/**
 * Created by marko on 7/28/15.
 */
public interface BoatDetailsInteractor {
    void sendVote(int voteId, int boatId, VoteListener listener);

    void addComment(Comment comment, long boatId, CommentListener commentListener);

    void getBoat(long boatId, BoatResponseListener listener);
}
