package co.infinum.stucademy.boatit.mvp.listeners;

/**
 * Created by marko on 7/29/15.
 */
public interface LoudLoginListener {

    void onLoginSuccessful();

    void onLoginFailed(String message);
}
