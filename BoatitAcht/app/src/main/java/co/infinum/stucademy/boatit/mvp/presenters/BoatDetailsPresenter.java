package co.infinum.stucademy.boatit.mvp.presenters;

import android.content.Intent;

import co.infinum.stucademy.boatit.models.Boat;
import co.infinum.stucademy.boatit.models.Comment;

/**
 * Created by marko on 7/28/15.
 */
public interface BoatDetailsPresenter {

    void onVoteClicked(int voteId, int boatId);

    void onIntentReceived(Boat boat);

    void onIntentDefect();

    void addComment(Comment comment, long id);

    void reloadBoat(long boatId);
}
