package co.infinum.stucademy.boatit.models.responses;

import com.google.gson.annotations.SerializedName;

/**
 * Created by marko on 7/21/15.
 */
public class LoginResponse {

    /**
     * {
     * "response": {
     * "token": "EXzEGryStDSxYwtDYyzf",
     * "first_name": "Admin",
     * "last_name": "Infinum",
     * "email": "admin@infinum.co"
     * }
     * }
     */

    @SerializedName("response")
    private Response response;

    public LoginResponse(Response response) {
        this.response = response;
    }

    public Response getResponse() {
        return response;
    }

    public class Response {
        @SerializedName("token")
        private String token;

        public Response(String token) {
            this.token = token;
        }

        public String getToken() {
            return token;
        }
    }

}
