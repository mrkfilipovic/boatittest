package co.infinum.stucademy.boatit.database;

import com.raizlabs.android.dbflow.annotation.Database;

/**
 * Created by marko on 7/25/15.
 */
@Database(name = BoatItDatabase.NAME, version = BoatItDatabase.VERSION)
public class BoatItDatabase {

    public static final String NAME = "BoatIt";

    public static final int VERSION = 1;
}