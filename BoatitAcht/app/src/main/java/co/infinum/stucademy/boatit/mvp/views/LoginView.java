package co.infinum.stucademy.boatit.mvp.views;

import co.infinum.stucademy.boatit.models.User;

/**
 * Created by marko on 7/29/15.
 */
public interface LoginView extends BaseView {

    void initData(User user);

    void showBoatList();

    void showRegistrationActivity();

    void showLoginError(String message);
}
