package co.infinum.stucademy.boatit.utilities;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import co.infinum.stucademy.boatit.BoatItApplication;

/**
 * Created by marko on 7/27/15.
 */
public class NetworkUtils {
    public static boolean connected() {
        ConnectivityManager connectivityManager = (ConnectivityManager) BoatItApplication.getInstance().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }
}
