package co.infinum.stucademy.boatit.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import co.infinum.stucademy.boatit.R;
import co.infinum.stucademy.boatit.interfaces.BoatListener;
import co.infinum.stucademy.boatit.models.Boat;

/**
 * Created by marko on 7/23/15.
 */
public class BoatsAdapter extends RecyclerView.Adapter<BoatsAdapter.BoatsViewHolder> {

    List<Boat> boatList;
    Context context;
    private BoatListener listener;

    public BoatsAdapter(List<Boat> boatList, Context context, BoatListener listener) {
        this.boatList = boatList;
        this.context = context;
        this.listener = listener;
    }

    @Override
    public BoatsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_boat, parent, false);
        return new BoatsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(BoatsViewHolder boatsViewHolder, int i) {

        final Boat boat = boatList.get(i);

        if (!TextUtils.isEmpty(boat.getBoatImage())) {
            Glide.with(context).load(boat.getBoatImage()).placeholder(R.drawable.ic_logo).dontAnimate().into(boatsViewHolder.boatImage).onLoadFailed(null, context.getResources().getDrawable(R.drawable.ic_logo));
        }
        boatsViewHolder.boatTitle.setText(boat.getBoatName());

        boatsViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onBoatClick(boat);
            }
        });
    }

    @Override
    public int getItemCount() {
        return boatList.size();
    }

    public class BoatsViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.boat_image)
        ImageView boatImage;
        @Bind(R.id.boat_title)
        TextView boatTitle;
        @Bind(R.id.boat_text_short)
        TextView boatShortText;

        public BoatsViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
