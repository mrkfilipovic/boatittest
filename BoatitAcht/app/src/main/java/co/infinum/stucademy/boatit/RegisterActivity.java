package co.infinum.stucademy.boatit;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.infinum.stucademy.boatit.models.User;
import co.infinum.stucademy.boatit.models.requests.RegisterRequest;
import co.infinum.stucademy.boatit.models.responses.LoginResponse;
import co.infinum.stucademy.boatit.network.ApiManagerImpl;
import co.infinum.stucademy.boatit.utilities.SharedPreferencesUtils;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class RegisterActivity extends AppCompatActivity {

    @Bind(R.id.progress_bar)
    ProgressBar progressBar;

    @Bind(R.id.username)
    EditText usernameEditText;

    @Bind(R.id.password)
    EditText passwordEditText;

    @Bind(R.id.password_repeat)
    EditText passwordRepeatEditText;

    @Bind(R.id.firstname)
    EditText firstnameEditText;
    @Bind(R.id.lastname)
    EditText lastnameEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);

        progressBar.setVisibility(View.INVISIBLE);
    }

    @OnClick(R.id.register)
    protected void register() {

        String firstname = firstnameEditText.getText().toString();
        String lastname = lastnameEditText.getText().toString();
        String username = usernameEditText.getText().toString();
        String password = passwordEditText.getText().toString();
        String passwordRepeat = passwordRepeatEditText.getText().toString();

        boolean dataCorrect = checkEnteredData(firstname, lastname, username, password, passwordRepeat);

        if (dataCorrect) {
            User user = new User(username, password, passwordRepeat, firstname, lastname);
            BoatItApplication.getApiService().register(new RegisterRequest(user), new Callback<LoginResponse>() {

                @Override
                public void success(LoginResponse loginResponse, Response response) {
                    Toast.makeText(getApplicationContext(), "registrated! : " + loginResponse.getResponse().getToken(), Toast.LENGTH_LONG).show();
                    login(loginResponse.getResponse().getToken());
                }

                @Override
                public void failure(RetrofitError error) {
                    Toast.makeText(getApplicationContext(), "error:" + error.getMessage(), Toast.LENGTH_LONG).show();

                }
            });
        }

    }

    private void login(String token) {
        SharedPreferencesUtils.setToken(token, this);
        setResult(RESULT_OK);
        finish();
    }

    private boolean checkEnteredData(String firstName, String lastname, String username, String password, String passwordRepeat) {
        boolean dataOk = true;

        if (TextUtils.isEmpty(firstName)) {
            firstnameEditText.setError("Cannot be empty.");
            dataOk = false;
        }

        if (TextUtils.isEmpty(firstName)) {
            lastnameEditText.setError("Cannot be empty.");
            dataOk = false;
        }

        if (TextUtils.isEmpty(username)) {
            usernameEditText.setError("Username not set.");
            dataOk = false;
        }

        if (TextUtils.isEmpty(password)) {
            passwordEditText.setError("Password not set.");
            dataOk = false;
        }

        if (TextUtils.isEmpty(passwordRepeat)) {
            passwordRepeatEditText.setError("Please repeat the password.");
            dataOk = false;
        }

        if (!TextUtils.equals(password, passwordRepeat)) {
            passwordRepeatEditText.setError("Password mismatch.");
            passwordEditText.requestFocus();
            dataOk = false;
        }

        return dataOk;
    }

}
