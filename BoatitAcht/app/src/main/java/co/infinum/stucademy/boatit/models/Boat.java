package co.infinum.stucademy.boatit.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.ForeignKey;
import com.raizlabs.android.dbflow.annotation.ForeignKeyReference;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.util.ArrayList;
import java.util.List;

import co.infinum.stucademy.boatit.database.BoatItDatabase;

/**
 * Created by marko on 7/23/15.
 */
@Table(databaseName = BoatItDatabase.NAME)
public class Boat extends BaseModel implements Parcelable {

    public static final Parcelable.Creator<Boat> CREATOR = new Parcelable.Creator<Boat>() {
        public Boat createFromParcel(Parcel source) {
            return new Boat(source);
        }

        public Boat[] newArray(int size) {
            return new Boat[size];
        }
    };

    @Column
    @PrimaryKey(autoincrement = true)
    @SerializedName("id")
    long id;
    @Column
    @SerializedName("title")
    String boatName;
    @Column
    @SerializedName("image_url")
    String boatImage;
    @Column
    @SerializedName("score")
    long score;
    @Column
    @ForeignKey(references = {@ForeignKeyReference(columnName = "creator_id",
            columnType = Long.class,
            foreignColumnName = "id")})
    @SerializedName("creator")
    User creator;
    @SerializedName("comments")
    private List<Comment> comments;

    public Boat() {
    }

    protected Boat(Parcel in) {
        this.id = in.readLong();
        this.boatName = in.readString();
        this.boatImage = in.readString();
        this.score = in.readLong();
        this.creator = in.readParcelable(User.class.getClassLoader());
        this.comments = new ArrayList<Comment>();
        in.readList(this.comments, Comment.class.getClassLoader());
    }

    public Boat(String name, String imageUrl) {
        this.boatName = name;
        this.boatImage = imageUrl;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public User getCreator() {
        return creator;
    }

    public long getScore() {
        return score;
    }

    public String getBoatImage() {
        return boatImage;
    }

    public String getBoatName() {
        return boatName;
    }

    public long getId() {
        return id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.boatName);
        dest.writeString(this.boatImage);
        dest.writeLong(this.score);
        dest.writeParcelable(this.creator, flags);
        dest.writeList(this.comments);
    }
}
