package co.infinum.stucademy.boatit.mvp.interactors;

import android.content.Context;

import co.infinum.stucademy.boatit.mvp.listeners.LoudLoginListener;
import co.infinum.stucademy.boatit.mvp.listeners.RememberListener;

/**
 * Created by marko on 7/29/15.
 */
public interface LoginInteractor {

    void checkLoginCredentials(String username, String password, boolean remember, Context context, LoudLoginListener listener);

    void checkRememberance(Context context, RememberListener rememberListener);
}
