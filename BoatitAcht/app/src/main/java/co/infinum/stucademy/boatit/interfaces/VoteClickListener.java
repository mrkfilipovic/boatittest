package co.infinum.stucademy.boatit.interfaces;

import android.view.View;

/**
 * Created by marko on 7/24/15.
 */
public interface VoteClickListener {

    void voteClicked(View view);
}
