package co.infinum.stucademy.boatit.mvp.listeners;

import java.util.List;

import co.infinum.stucademy.boatit.models.Boat;

/**
 * Created by marko on 7/27/15.
 */
public interface BoatsListener {

    void onBoatsReceived(List<Boat> boats);

    void onListEmpty();
}
