package co.infinum.stucademy.boatit.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.Toast;

import co.infinum.stucademy.boatit.R;

/**
 * Created by marko on 7/25/15.
 */
public class BaseActivity extends AppCompatActivity {

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onStop() {
        super.onStop();
        hideLoader();
    }

    void setBackEnabled(boolean enabled) {
        if (getActionBar() != null) {
            getActionBar().setDisplayHomeAsUpEnabled(enabled);
        }
    }

    @Override
    public void setTitle(CharSequence title) {
        super.setTitle(title);
    }

    protected void showLoader() {
        if (progressDialog == null || !progressDialog.isShowing()) {
            progressDialog = ProgressDialog.show(this, getString(R.string.app_name), getString(R.string.please_wait), true, false);
        }
    }

    protected void hideLoader() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    protected void startNextActivity(Intent intent, boolean finish) {
        startActivity(intent);
        if (finish) {
            finish();
        }
    }

    protected void showAlert(String message) {
        AlertDialog dialog = new AlertDialog.Builder(this).setMessage(message).create();
        dialog.setButton(DialogInterface.BUTTON_POSITIVE, getText(R.string.please_wait), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public void connectivityError() {
        Toast.makeText(this, R.string.no_register_connectivity, Toast.LENGTH_LONG).show();
    }

//    protected void startNextActivity(Activity current, Class next, int[] resId, Object... args) {
//        Intent intent = new Intent(current, next);
//        int i = 0;
//        for (Object obj : args) {
//            intent.putExtra(getString(resId[i++]), obj);
//        }
//        startActivity(intent);
//    }

}
