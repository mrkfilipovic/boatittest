package co.infinum.stucademy.boatit.mvp.presenters.impl;

import android.content.Context;

import co.infinum.stucademy.boatit.mvp.interactors.impl.SplashInteractorImpl;
import co.infinum.stucademy.boatit.mvp.listeners.SilentLoginListener;
import co.infinum.stucademy.boatit.mvp.presenters.SplashPresenter;
import co.infinum.stucademy.boatit.mvp.views.SplashView;

/**
 * Created by marko on 7/29/15.
 */
public class SplashPresenterImpl implements SplashPresenter {

    private final SplashView view;
    private final SplashInteractorImpl interactor;
    private Context context;

    public SplashPresenterImpl(SplashView view, SplashInteractorImpl splashInteractor, Context context) {
        this.view = view;
        this.interactor = splashInteractor;
        this.context = context;
    }

    @Override
    public void checkUserExists() {
        interactor.checkSavedData(context, listener);
    }

    private SilentLoginListener listener = new SilentLoginListener() {

        @Override
        public void userExists() {
            view.showBoatActivity();
        }

        @Override
        public void userNotFound() {
            view.showLoginActivity();
        }
    };

}
