package co.infinum.stucademy.boatit.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

import co.infinum.stucademy.boatit.database.BoatItDatabase;

/**
 * Created by marko on 7/21/15.
 */
@Table(databaseName = BoatItDatabase.NAME)
public class User extends BaseModel implements Parcelable {

    // TODO Unique users

    public static final Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>() {
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        public User[] newArray(int size) {
            return new User[size];
        }
    };
    @Column
    @PrimaryKey(autoincrement = true)
    long id;
    @SerializedName("email")
    String username;
    @SerializedName("password")
    String password;
    @SerializedName("password_confirmation")
    String passwordConfirmation;
    @Column(name = "first_name")
    @SerializedName("first_name")
    String firstName;
    @Column(name = "last_name")
    @SerializedName("last_name")
    String lastName;

    public User() {
    }

    public User(String username, String password, String passwordConfirmation) {
        this.username = username;
        this.password = password;
        this.passwordConfirmation = passwordConfirmation;
    }

    public User(String username, String password, String passwordConfirmation, String firstName, String lastName) {
        this.username = username;
        this.password = password;
        this.passwordConfirmation = passwordConfirmation;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    protected User(Parcel in) {
        this.id = in.readLong();
        this.username = in.readString();
        this.password = in.readString();
        this.passwordConfirmation = in.readString();
        this.firstName = in.readString();
        this.lastName = in.readString();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.username);
        dest.writeString(this.password);
        dest.writeString(this.passwordConfirmation);
        dest.writeString(this.firstName);
        dest.writeString(this.lastName);
    }

}