package co.infinum.stucademy.boatit.mvp.views;

/**
 * Created by marko on 7/27/15.
 */
public interface BaseView {

    void showProgress();

    void hideProgress();
    
    void setTitle(int title);

    void error();

    void noConnectivity();
}
