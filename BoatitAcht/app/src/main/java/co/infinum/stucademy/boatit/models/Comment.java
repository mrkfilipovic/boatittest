package co.infinum.stucademy.boatit.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.ForeignKey;
import com.raizlabs.android.dbflow.annotation.ForeignKeyReference;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import co.infinum.stucademy.boatit.database.BoatItDatabase;

/**
 * Created by marko on 7/23/15.
 */
@Table(databaseName = BoatItDatabase.NAME)
public class Comment extends BaseModel implements Parcelable {

    public static final Creator<Comment> CREATOR = new Creator<Comment>() {
        public Comment createFromParcel(Parcel source) {
            return new Comment(source);
        }

        public Comment[] newArray(int size) {
            return new Comment[size];
        }
    };
    private static final String DATE_FORMAT = "dd-MM-yyyy";
    @Column
    @PrimaryKey(autoincrement = true)
    @SerializedName("id")
    long id;
    @Column
    @SerializedName("content")
    String content;
    @Column(name = "date_created")
    @SerializedName("created_at")
    Date date;
    @Column
    @ForeignKey(references = {@ForeignKeyReference(columnName = "creator_id",
            columnType = Long.class,
            foreignColumnName = "id")})
    @SerializedName("author")
    User author;
    @Column
    @ForeignKey(references = {@ForeignKeyReference(columnName = "boat_id",
            columnType = Long.class,
            foreignColumnName = "id")})
    Boat boat;

    public Comment() {
    }

    public Comment(String content) {
        this.content = content;
    }

    protected Comment(Parcel in) {
        this.id = in.readLong();
        this.content = in.readString();
        long tmpDate = in.readLong();
        this.date = tmpDate == -1 ? null : new Date(tmpDate);
        this.author = in.readParcelable(User.class.getClassLoader());
    }

    public long getId() {
        return id;
    }

    public String getContent() {
        return content;
    }

    public Date getDate() {
        return date;
    }

    public User getAuthor() {
        return author;
    }

    public Boat getBoat() {
        return boat;
    }

    public void setBoat(Boat boat) {
        this.boat = boat;
    }

    public String getDateCreated() {
        SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT, Locale.GERMAN);
        return format.format(date);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.content);
        dest.writeLong(date != null ? date.getTime() : -1);
        dest.writeParcelable(this.author, 0);
    }
}
