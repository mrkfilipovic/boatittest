package co.infinum.stucademy.boatit.models.requests;

import com.google.gson.annotations.SerializedName;

import co.infinum.stucademy.boatit.models.User;

/**
 * Created by marko on 7/21/15.
 */
public class RegisterRequest {

    @SerializedName("user")
    private User user;

    public RegisterRequest(User user) {
        this.user = user;
    }
}
