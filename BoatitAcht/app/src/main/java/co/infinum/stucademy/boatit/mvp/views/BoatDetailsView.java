package co.infinum.stucademy.boatit.mvp.views;

import co.infinum.stucademy.boatit.models.Boat;

/**
 * Created by marko on 7/28/15.
 */
public interface BoatDetailsView extends BaseView {

    void setupVotes(int score, int viewId);

    void setTitle(int title, String boatName);

    void onBoatFound(Boat boat);

    void reloadComments();

    void setupView(Boat boat);
}
