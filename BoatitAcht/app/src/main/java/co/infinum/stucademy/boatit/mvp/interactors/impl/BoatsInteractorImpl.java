package co.infinum.stucademy.boatit.mvp.interactors.impl;

import java.util.List;

import co.infinum.stucademy.boatit.BoatItApplication;
import co.infinum.stucademy.boatit.database.BoatDBOperationsImpl;
import co.infinum.stucademy.boatit.models.Boat;
import co.infinum.stucademy.boatit.models.requests.BoatRequest;
import co.infinum.stucademy.boatit.models.responses.BoatsResponse;
import co.infinum.stucademy.boatit.mvp.interactors.BoatsInteractor;
import co.infinum.stucademy.boatit.mvp.listeners.BoatAddedListener;
import co.infinum.stucademy.boatit.mvp.listeners.BoatsListener;
import co.infinum.stucademy.boatit.network.ApiManagerImpl;
import co.infinum.stucademy.boatit.utilities.NetworkUtils;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by marko on 7/27/15.
 */
public class BoatsInteractorImpl implements BoatsInteractor {

    private BoatsListener listener;

    @Override
    public void getBoats(BoatsListener listener) {
        this.listener = listener;

        if (NetworkUtils.connected()) {
            BoatItApplication.getApiService().getBoats(boatResponseCallback);
        } else {
            lootDB();
        }
    }

    @Override
    public void addNewBoat(Boat boat, final BoatAddedListener boatAddedListener) {
        BoatItApplication.getApiService().addBoat(new BoatRequest(boat), new Callback<com.squareup.okhttp.Response>() {
            @Override
            public void success(com.squareup.okhttp.Response response, Response response2) {
                boatAddedListener.onBoatAddedSuccess();
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    private void lootDB() {
        BoatDBOperationsImpl dbOperations = new BoatDBOperationsImpl();
        List<Boat> boatList = dbOperations.getAllBoats();
        checkList(boatList, true);
    }

    private void checkList(List<Boat> boatList, boolean fromDB) {
        if (boatList != null && boatList.size() > 0) {
            if (!fromDB) {
                saveBoats(boatList);
            }
            listener.onBoatsReceived(boatList);
        } else {
            listener.onListEmpty();
        }
    }

    private void saveBoats(List<Boat> boatList) {
        BoatDBOperationsImpl dbOperations = new BoatDBOperationsImpl();
        dbOperations.addBoats(boatList);
    }


    private Callback<BoatsResponse> boatResponseCallback = new Callback<BoatsResponse>() {
        @Override
        public void success(BoatsResponse boatResponse, Response response) {
            checkList(boatResponse.getBoats(), false);
        }

        @Override
        public void failure(RetrofitError error) {

        }
    };
}
