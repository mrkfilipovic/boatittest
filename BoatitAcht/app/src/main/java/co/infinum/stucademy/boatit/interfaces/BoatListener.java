package co.infinum.stucademy.boatit.interfaces;

import co.infinum.stucademy.boatit.models.Boat;

/**
 * Created by marko on 7/23/15.
 */
public interface BoatListener {

    void onBoatClick(Boat boat);
}
