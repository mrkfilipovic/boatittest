package co.infinum.stucademy.boatit.mvp.interactors.impl;

import android.content.Context;

import java.net.HttpURLConnection;

import co.infinum.stucademy.boatit.BoatItApplication;
import co.infinum.stucademy.boatit.models.User;
import co.infinum.stucademy.boatit.models.requests.LoginRequest;
import co.infinum.stucademy.boatit.models.responses.LoginResponse;
import co.infinum.stucademy.boatit.mvp.interactors.SplashInteractor;
import co.infinum.stucademy.boatit.mvp.listeners.SilentLoginListener;
import co.infinum.stucademy.boatit.network.ApiManagerImpl;
import co.infinum.stucademy.boatit.utilities.SharedPreferencesUtils;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by marko on 7/29/15.
 */
public class SplashInteractorImpl implements SplashInteractor {
    @Override
    public void checkSavedData(final Context context, final SilentLoginListener listener) {
        if (SharedPreferencesUtils.getRemember(context)) {
            User user = SharedPreferencesUtils.getUser(context);
            if (user != null) {
                BoatItApplication.getApiService().login(new LoginRequest(user.getUsername(), user.getPassword()), new Callback<LoginResponse>() {
                    @Override
                    public void success(LoginResponse loginResponse, Response response) {
                        if (response.getStatus() == HttpURLConnection.HTTP_OK) {
                            listener.userExists();
                            SharedPreferencesUtils.setToken(loginResponse.getResponse().getToken(), context);
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        listener.userNotFound();
                    }
                });
            }
        } else {
            listener.userNotFound();
        }
    }
}
