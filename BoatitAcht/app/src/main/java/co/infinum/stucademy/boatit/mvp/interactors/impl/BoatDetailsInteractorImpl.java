package co.infinum.stucademy.boatit.mvp.interactors.impl;

import com.squareup.okhttp.Response;

import java.net.HttpURLConnection;

import co.infinum.stucademy.boatit.BoatItApplication;
import co.infinum.stucademy.boatit.R;
import co.infinum.stucademy.boatit.models.Comment;
import co.infinum.stucademy.boatit.models.requests.CommentRequest;
import co.infinum.stucademy.boatit.models.responses.BoatResponse;
import co.infinum.stucademy.boatit.models.responses.VoteResponse;
import co.infinum.stucademy.boatit.mvp.interactors.BoatDetailsInteractor;
import co.infinum.stucademy.boatit.mvp.listeners.BoatResponseListener;
import co.infinum.stucademy.boatit.mvp.listeners.CommentListener;
import co.infinum.stucademy.boatit.mvp.listeners.VoteListener;
import retrofit.Callback;
import retrofit.RetrofitError;

/**
 * Created by marko on 7/28/15.
 */
public class BoatDetailsInteractorImpl implements BoatDetailsInteractor {
    @Override
    public void sendVote(final int voteId, int boatId, final VoteListener listener) {
        switch (voteId) {
            case R.id.boat_upvote:
                BoatItApplication.getApiService().boatUpvote(boatId, new Callback<VoteResponse>() {

                    @Override
                    public void success(VoteResponse voteResponse, retrofit.client.Response response) {
                        if (response.getStatus() == HttpURLConnection.HTTP_OK) {
                            // TODO update boat in DB
                            listener.onVoteResponse(voteResponse.getResponse().getScore(), voteId);
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {

                    }
                });
                break;
            case R.id.boat_downvote:
                BoatItApplication.getApiService().boatDownvote(boatId, new Callback<VoteResponse>() {
                    @Override
                    public void success(VoteResponse voteResponse, retrofit.client.Response response2) {
                        if (response2.getStatus() == HttpURLConnection.HTTP_OK) {
                            // TODO update boat in DB
                            listener.onVoteResponse(voteResponse.getResponse().getScore(), voteId);
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {

                    }
                });
                break;
        }
    }

    @Override
    public void addComment(Comment comment, long boatId, final CommentListener commentListener) {
        BoatItApplication.getApiService().addComment(boatId, new CommentRequest(comment), new Callback<Response>() {

            @Override
            public void success(Response response, retrofit.client.Response response2) {
                commentListener.commentAdded();
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    @Override
    public void getBoat(long boatId, final BoatResponseListener listener) {
        BoatItApplication.getApiService().getBoat(boatId, new Callback<BoatResponse>() {
            @Override
            public void success(BoatResponse boatResponse, retrofit.client.Response response) {
                listener.onBoatReceived(boatResponse.getBoat());
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }
}
