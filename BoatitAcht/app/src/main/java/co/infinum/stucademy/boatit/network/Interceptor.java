package co.infinum.stucademy.boatit.network;

import android.text.TextUtils;

import co.infinum.stucademy.boatit.BoatItApplication;
import co.infinum.stucademy.boatit.utilities.SharedPreferencesUtils;
import retrofit.RequestInterceptor;

/**
 * Created by marko on 7/21/15.
 */
public class Interceptor implements RequestInterceptor {
    private static final String TOKEN = "token";

    @Override
    public void intercept(RequestFacade request) {
        String token = SharedPreferencesUtils.getToken(BoatItApplication.getInstance());
        if (!TextUtils.isEmpty(token)) {
            request.addQueryParam(TOKEN, token);
        }
    }
}
