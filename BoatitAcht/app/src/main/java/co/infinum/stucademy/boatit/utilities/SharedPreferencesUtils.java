package co.infinum.stucademy.boatit.utilities;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import co.infinum.stucademy.boatit.models.User;

/**
 * Created by marko on 7/21/15.
 */
public class SharedPreferencesUtils {

    private static final String TOKEN = "token";
    private static final String PASSWORD = "password";
    private static final String USERNAME = "username";
    private static final String REMEMBER = "remember";

    private static SharedPreferences getPreferences(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static String getToken(Context context) {
        SharedPreferences preferences = getPreferences(context);
        return preferences.getString(TOKEN, null);
    }

    public static void setToken(String token, Context context) {
        SharedPreferences preferences = getPreferences(context);
        preferences.edit().putString(TOKEN, token).apply();
    }

    public static User getUser(Context context) {
        SharedPreferences preferences = getPreferences(context);
        String password = preferences.getString(PASSWORD, null);
        String username = preferences.getString(USERNAME, null);
        if (username != null && password != null) {
            return new User(username, password);
        }
        return null;
    }

    public static void saveUser(User user, Context context) {
        SharedPreferences preferences = getPreferences(context);
        preferences.edit().putString(USERNAME, user.getUsername()).apply();
        preferences.edit().putString(PASSWORD, user.getPassword()).apply();
    }

    public static void rememberUser(boolean remember, Context context) {
        SharedPreferences preferences = getPreferences(context);
        preferences.edit().putBoolean(REMEMBER, remember).apply();
    }

    public static boolean getRemember(Context context) {
        SharedPreferences preferences = getPreferences(context);
        return preferences.getBoolean(REMEMBER, false);

    }
}
