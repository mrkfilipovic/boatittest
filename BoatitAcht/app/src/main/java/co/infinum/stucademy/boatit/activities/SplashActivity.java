package co.infinum.stucademy.boatit.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import co.infinum.stucademy.boatit.R;
import co.infinum.stucademy.boatit.factories.MvpFactory;
import co.infinum.stucademy.boatit.mvp.presenters.SplashPresenter;
import co.infinum.stucademy.boatit.mvp.views.SplashView;

public class SplashActivity extends BaseActivity implements SplashView {

    private static final long SPLASH_DURATION = 3000;

    private SplashPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        setupPresenter();
        delay();
    }

    private void setupPresenter() {
        presenter = MvpFactory.getPresenter(this, this);
    }

    private void delay() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                presenter.checkUserExists();
            }
        }, SPLASH_DURATION);
    }

    @Override
    public void showBoatActivity() {
        startScreen(BoatsActivity.class);
    }

    @Override
    public void showLoginActivity() {
        startScreen(LoginActivity.class);
    }

    private void startScreen(Class<? extends Activity> c) {
        Intent intent = new Intent(this, c);
        startActivity(intent);
        finish();
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void error() {

    }

    @Override
    public void noConnectivity() {
        super.connectivityError();
    }
}
