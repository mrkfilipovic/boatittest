package co.infinum.stucademy.boatit.mvp.interactors;

import co.infinum.stucademy.boatit.models.Boat;
import co.infinum.stucademy.boatit.mvp.listeners.BoatAddedListener;
import co.infinum.stucademy.boatit.mvp.listeners.BoatsListener;

/**
 * Created by marko on 7/27/15.
 */
public interface BoatsInteractor {

    void getBoats(BoatsListener listener);

    void addNewBoat(Boat boat, BoatAddedListener boatAddedListener);
}
