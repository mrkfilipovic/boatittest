package co.infinum.stucademy.boatit.models.responses;

import com.google.gson.annotations.SerializedName;

import co.infinum.stucademy.boatit.models.Boat;

/**
 * Created by marko on 7/28/15.
 */
public class VoteResponse {
    @SerializedName("response")
    private Response response;

    public Response getResponse() {
        return response;
    }

    public class Response {
        @SerializedName("score")
        private int score;

        public Response(int score) {
            this.score = score;
        }

        public int getScore() {
            return score;
        }
    }
}
