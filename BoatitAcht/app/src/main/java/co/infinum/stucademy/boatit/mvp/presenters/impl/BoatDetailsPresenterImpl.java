package co.infinum.stucademy.boatit.mvp.presenters.impl;

import co.infinum.stucademy.boatit.R;
import co.infinum.stucademy.boatit.models.Boat;
import co.infinum.stucademy.boatit.models.Comment;
import co.infinum.stucademy.boatit.mvp.interactors.BoatDetailsInteractor;
import co.infinum.stucademy.boatit.mvp.listeners.BoatResponseListener;
import co.infinum.stucademy.boatit.mvp.listeners.CommentListener;
import co.infinum.stucademy.boatit.mvp.listeners.VoteListener;
import co.infinum.stucademy.boatit.mvp.presenters.BoatDetailsPresenter;
import co.infinum.stucademy.boatit.mvp.views.BoatDetailsView;

/**
 * Created by marko on 7/28/15.
 */
public class BoatDetailsPresenterImpl implements BoatDetailsPresenter {

    private BoatDetailsInteractor interactor;
    private BoatDetailsView view;

    public BoatDetailsPresenterImpl(BoatDetailsView view, BoatDetailsInteractor boatDetailsInteractor) {
        this.interactor = boatDetailsInteractor;
        this.view = view;
    }

    @Override
    public void onVoteClicked(int voteId, int boatId) {
        interactor.sendVote(voteId, boatId, listener);
    }

    private VoteListener listener = new VoteListener() {
        @Override
        public void onVoteResponse(int score, int viewId) {
            view.setupVotes(score, viewId);
        }
    };

    @Override
    public void onIntentReceived(Boat boat) {
        if (boat != null) {
            view.showProgress();
            view.setupView(boat);
            view.setTitle(R.string.title_boat_details_activity, boat.getBoatName());
            view.hideProgress();
        }
    }

    @Override
    public void addComment(Comment comment, long id) {
        interactor.addComment(comment, id, commentListener);
    }

    private CommentListener commentListener = new CommentListener() {
        @Override
        public void commentAdded() {
            view.reloadComments();
        }
    };

    @Override
    public void reloadBoat(long boatId) {
        interactor.getBoat(boatId, boatListener);
    }

    private BoatResponseListener boatListener = new BoatResponseListener() {
        @Override
        public void onBoatReceived(Boat boat) {
            view.onBoatFound(boat);
        }
    };

    @Override
    public void onIntentDefect() {
        view.hideProgress();
        view.error();
    }
}

