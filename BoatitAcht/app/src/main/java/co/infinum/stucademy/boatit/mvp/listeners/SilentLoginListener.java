package co.infinum.stucademy.boatit.mvp.listeners;

/**
 * Created by marko on 7/29/15.
 */
public interface SilentLoginListener {

    void userExists();

    void userNotFound();
}
