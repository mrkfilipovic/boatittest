package co.infinum.stucademy.boatit.mvp.presenters;

import co.infinum.stucademy.boatit.models.Boat;

/**
 * Created by marko on 7/27/15.
 */
public interface BoatsPresenter {

    void getBoats();

    void onBoatClicked(Boat boat);

    void addNewBoat(Boat boat);
}
