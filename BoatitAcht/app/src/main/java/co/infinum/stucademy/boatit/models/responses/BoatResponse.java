package co.infinum.stucademy.boatit.models.responses;

import com.google.gson.annotations.SerializedName;

import co.infinum.stucademy.boatit.models.Boat;

/**
 * Created by marko on 7/28/15.
 */
public class BoatResponse {

    @SerializedName("response")
    private Boat boat;

    public Boat getBoat() {
        return boat;
    }
}
