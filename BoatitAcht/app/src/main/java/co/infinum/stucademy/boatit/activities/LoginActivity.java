package co.infinum.stucademy.boatit.activities;

import android.content.Intent;
import android.os.Bundle;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnLongClick;
import co.infinum.stucademy.boatit.R;
import co.infinum.stucademy.boatit.RegisterActivity;
import co.infinum.stucademy.boatit.factories.MvpFactory;
import co.infinum.stucademy.boatit.models.User;
import co.infinum.stucademy.boatit.mvp.presenters.LoginPresenter;
import co.infinum.stucademy.boatit.mvp.views.LoginView;
import co.infinum.stucademy.boatit.utilities.NetworkUtils;
import co.infinum.stucademy.boatit.utilities.ViewsUtils;

public class LoginActivity extends BaseActivity implements LoginView {

    private static final int REGISTRATION = 1;

    @Bind(R.id.username)
    EditText usernameEditText;

    @Bind(R.id.password)
    EditText passwordEditText;

    @Bind(R.id.remember)
    CheckBox remember;

    private LoginPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        setupPresenter();
    }

    private void setupPresenter() {
        presenter = MvpFactory.getPresenter(this, this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        init();
    }

    private void init() {
        if (!NetworkUtils.connected()) {
            Toast.makeText(getBaseContext(), R.string.network_unavailable, Toast.LENGTH_LONG).show();
        }
        presenter.isRemembered();
    }

    @Override
    public void initData(User user) {
        if (user != null) {
            usernameEditText.setText(user.getUsername());
            passwordEditText.setText(user.getPassword());
            remember.setSelected(true);
            remember.setChecked(true);
        }
    }

    @OnLongClick(R.id.username)
    protected boolean usernameLongClick() {

        //TODO delete this part of the method
        usernameEditText.setText("malik@duhovi.hr");
        passwordEditText.setText("tintilinic");
        return true;
    }

    @OnClick(R.id.login)
    protected void login() {
        presenter.login(ViewsUtils.getTextFrom(usernameEditText), ViewsUtils.getTextFrom(passwordEditText), remember.isChecked());
    }


    @Override
    public void showLoginError(String message) {
        showAlert(message);
    }

    @Override
    public void noConnectivity() {
        super.connectivityError();
    }

    @Override
    public void showRegistrationActivity() {
        startActivityForResult(new Intent(LoginActivity.this, RegisterActivity.class), REGISTRATION);
    }

    @Override
    public void showBoatList() {
        super.startNextActivity(new Intent(LoginActivity.this, BoatsActivity.class), true);
    }

    @OnClick(R.id.register)
    protected void register() {
        presenter.registration();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REGISTRATION && resultCode == RESULT_OK) {
            showBoatList();
        }
    }

    @Override
    public void showProgress() {
        showLoader();
    }

    @Override
    public void hideProgress() {
        hideLoader();
    }

    @Override
    public void error() {
        showAlert(getString(R.string.error_login));
    }
}
